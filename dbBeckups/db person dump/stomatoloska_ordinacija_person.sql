-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: stomatoloska_ordinacija
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `birth_date` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  `phone_number` varchar(45) NOT NULL,
  `upper_left1healthy` varchar(45) DEFAULT NULL,
  `upper_left1tooth_decay` varchar(45) DEFAULT NULL,
  `upper_left1repaired` varchar(45) DEFAULT NULL,
  `upper_left1extracted` varchar(45) DEFAULT NULL,
  `upper_left2healthy` varchar(45) DEFAULT NULL,
  `upper_left2tooth_decay` varchar(45) DEFAULT NULL,
  `upper_left2repaired` varchar(45) DEFAULT NULL,
  `upper_left2extracted` varchar(45) DEFAULT NULL,
  `upper_left3healthy` varchar(45) DEFAULT NULL,
  `upper_left3tooth_decay` varchar(45) DEFAULT NULL,
  `upper_left3repaired` varchar(45) DEFAULT NULL,
  `upper_left3extracted` varchar(45) DEFAULT NULL,
  `upper_left4healthy` varchar(45) DEFAULT NULL,
  `upper_left4tooth_decay` varchar(45) DEFAULT NULL,
  `upper_left4repaired` varchar(45) DEFAULT NULL,
  `upper_left4extracted` varchar(45) DEFAULT NULL,
  `upper_left5healthy` varchar(45) DEFAULT NULL,
  `upper_left5tooth_decay` varchar(45) DEFAULT NULL,
  `upper_left5repaired` varchar(45) DEFAULT NULL,
  `upper_left5extracted` varchar(45) DEFAULT NULL,
  `upper_left6healthy` varchar(45) DEFAULT NULL,
  `upper_left6tooth_decay` varchar(45) DEFAULT NULL,
  `upper_left6repaired` varchar(45) DEFAULT NULL,
  `upper_left6extracted` varchar(45) DEFAULT NULL,
  `upper_left7healthy` varchar(45) DEFAULT NULL,
  `upper_left7tooth_decay` varchar(45) DEFAULT NULL,
  `upper_left7repaired` varchar(45) DEFAULT NULL,
  `upper_left7extracted` varchar(45) DEFAULT NULL,
  `upper_left8healthy` varchar(45) DEFAULT NULL,
  `upper_left8tooth_decay` varchar(45) DEFAULT NULL,
  `upper_left8repaired` varchar(45) DEFAULT NULL,
  `upper_left8extracted` varchar(45) DEFAULT NULL,
  `upper_right1healthy` varchar(45) DEFAULT NULL,
  `upper_right1tooth_decay` varchar(45) DEFAULT NULL,
  `upper_right1repaired` varchar(45) DEFAULT NULL,
  `upper_right1extracted` varchar(45) DEFAULT NULL,
  `upper_right2healthy` varchar(45) DEFAULT NULL,
  `upper_right2tooth_decay` varchar(45) DEFAULT NULL,
  `upper_right2repaired` varchar(45) DEFAULT NULL,
  `upper_right2extracted` varchar(45) DEFAULT NULL,
  `upper_right3healthy` varchar(45) DEFAULT NULL,
  `upper_right3tooth_decay` varchar(45) DEFAULT NULL,
  `upper_right3repaired` varchar(45) DEFAULT NULL,
  `upper_right3extracted` varchar(45) DEFAULT NULL,
  `upper_right4healthy` varchar(45) DEFAULT NULL,
  `upper_right4tooth_decay` varchar(45) DEFAULT NULL,
  `upper_right4repaired` varchar(45) DEFAULT NULL,
  `upper_right4extracted` varchar(45) DEFAULT NULL,
  `upper_right5healthy` varchar(45) DEFAULT NULL,
  `upper_right5tooth_decay` varchar(45) DEFAULT NULL,
  `upper_right5repaired` varchar(45) DEFAULT NULL,
  `upper_right5extracted` varchar(45) DEFAULT NULL,
  `upper_right6healthy` varchar(45) DEFAULT NULL,
  `upper_right6tooth_decay` varchar(45) DEFAULT NULL,
  `upper_right6repaired` varchar(45) DEFAULT NULL,
  `upper_right6extracted` varchar(45) DEFAULT NULL,
  `upper_right7healthy` varchar(45) DEFAULT NULL,
  `upper_right7tooth_decay` varchar(45) DEFAULT NULL,
  `upper_right7repaired` varchar(45) DEFAULT NULL,
  `upper_right7extracted` varchar(45) DEFAULT NULL,
  `upper_right8healthy` varchar(45) DEFAULT NULL,
  `upper_right8tooth_decay` varchar(45) DEFAULT NULL,
  `upper_right8repaired` varchar(45) DEFAULT NULL,
  `upper_right8extracted` varchar(45) DEFAULT NULL,
  `lower_left1healthy` varchar(45) DEFAULT NULL,
  `lower_left1tooth_decay` varchar(45) DEFAULT NULL,
  `lower_left1repaired` varchar(45) DEFAULT NULL,
  `lower_left1extracted` varchar(45) DEFAULT NULL,
  `lower_left2healthy` varchar(45) DEFAULT NULL,
  `lower_left2tooth_decay` varchar(45) DEFAULT NULL,
  `lower_left2repaired` varchar(45) DEFAULT NULL,
  `lower_left2extracted` varchar(45) DEFAULT NULL,
  `lower_left3healthy` varchar(45) DEFAULT NULL,
  `lower_left3tooth_decay` varchar(45) DEFAULT NULL,
  `lower_left3repaired` varchar(45) DEFAULT NULL,
  `lower_left3extracted` varchar(45) DEFAULT NULL,
  `lower_left4healthy` varchar(45) DEFAULT NULL,
  `lower_left4tooth_decay` varchar(45) DEFAULT NULL,
  `lower_left4repaired` varchar(45) DEFAULT NULL,
  `lower_left4extracted` varchar(45) DEFAULT NULL,
  `lower_left5healthy` varchar(45) DEFAULT NULL,
  `lower_left5tooth_decay` varchar(45) DEFAULT NULL,
  `lower_left5repaired` varchar(45) DEFAULT NULL,
  `lower_left5extracted` varchar(45) DEFAULT NULL,
  `lower_left6healthy` varchar(45) DEFAULT NULL,
  `lower_left6tooth_decay` varchar(45) DEFAULT NULL,
  `lower_left6repaired` varchar(45) DEFAULT NULL,
  `lower_left6extracted` varchar(45) DEFAULT NULL,
  `lower_left7healthy` varchar(45) DEFAULT NULL,
  `lower_left7tooth_decay` varchar(45) DEFAULT NULL,
  `lower_left7repaired` varchar(45) DEFAULT NULL,
  `lower_left7extracted` varchar(45) DEFAULT NULL,
  `lower_left8healthy` varchar(45) DEFAULT NULL,
  `lower_left8tooth_decay` varchar(45) DEFAULT NULL,
  `lower_left8repaired` varchar(45) DEFAULT NULL,
  `lower_left8extracted` varchar(45) DEFAULT NULL,
  `lower_right1healthy` varchar(45) DEFAULT NULL,
  `lower_right1tooth_decay` varchar(45) DEFAULT NULL,
  `lower_right1repaired` varchar(45) DEFAULT NULL,
  `lower_right1extracted` varchar(45) DEFAULT NULL,
  `lower_right2healthy` varchar(45) DEFAULT NULL,
  `lower_right2tooth_decay` varchar(45) DEFAULT NULL,
  `lower_right2repaired` varchar(45) DEFAULT NULL,
  `lower_right2extracted` varchar(45) DEFAULT NULL,
  `lower_right3healthy` varchar(45) DEFAULT NULL,
  `lower_right3tooth_decay` varchar(45) DEFAULT NULL,
  `lower_right3repaired` varchar(45) DEFAULT NULL,
  `lower_right3extracted` varchar(45) DEFAULT NULL,
  `lower_right4healthy` varchar(45) DEFAULT NULL,
  `lower_right4tooth_decay` varchar(45) DEFAULT NULL,
  `lower_right4repaired` varchar(45) DEFAULT NULL,
  `lower_right4extracted` varchar(45) DEFAULT NULL,
  `lower_right5healthy` varchar(45) DEFAULT NULL,
  `lower_right5tooth_decay` varchar(45) DEFAULT NULL,
  `lower_right5repaired` varchar(45) DEFAULT NULL,
  `lower_right5extracted` varchar(45) DEFAULT NULL,
  `lower_right6healthy` varchar(45) DEFAULT NULL,
  `lower_right6tooth_decay` varchar(45) DEFAULT NULL,
  `lower_right6repaired` varchar(45) DEFAULT NULL,
  `lower_right6extracted` varchar(45) DEFAULT NULL,
  `lower_right7healthy` varchar(45) DEFAULT NULL,
  `lower_right7tooth_decay` varchar(45) DEFAULT NULL,
  `lower_right7repaired` varchar(45) DEFAULT NULL,
  `lower_right7extracted` varchar(45) DEFAULT NULL,
  `lower_right8healthy` varchar(45) DEFAULT NULL,
  `lower_right8tooth_decay` varchar(45) DEFAULT NULL,
  `lower_right8repaired` varchar(45) DEFAULT NULL,
  `lower_right8extracted` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'vvv','bbbb','20-8-2020','vvv','ffff','444 555 666','true',NULL,'false',NULL,'false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','true',NULL,'false',NULL,'false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','true',NULL,'false',NULL,'false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','true',NULL,'false',NULL,'false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false','false');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-25 14:26:19
