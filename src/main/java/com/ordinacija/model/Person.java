package com.ordinacija.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private String birthDate;
    private String city;
    private String country;
    private String phoneNumber;

    private String upperLeft1Healthy;
    private String upperLeft1ToothDecay;
    private String upperLeft1Repaired;
    private String upperLeft1Extracted;

    private String upperLeft2Healthy;
    private String upperLeft2ToothDecay;
    private String upperLeft2Repaired;
    private String upperLeft2Extracted;

    private String upperLeft3Healthy;
    private String upperLeft3ToothDecay;
    private String upperLeft3Repaired;
    private String upperLeft3Extracted;

    private String upperLeft4Healthy;
    private String upperLeft4ToothDecay;
    private String upperLeft4Repaired;
    private String upperLeft4Extracted;

    private String upperLeft5Healthy;
    private String upperLeft5ToothDecay;
    private String upperLeft5Repaired;
    private String upperLeft5Extracted;

    private String upperLeft6Healthy;
    private String upperLeft6ToothDecay;
    private String upperLeft6Repaired;
    private String upperLeft6Extracted;

    private String upperLeft7Healthy;
    private String upperLeft7ToothDecay;
    private String upperLeft7Repaired;
    private String upperLeft7Extracted;

    private String upperLeft8Healthy;
    private String upperLeft8ToothDecay;
    private String upperLeft8Repaired;
    private String upperLeft8Extracted;

    private String upperRight1Healthy;
    private String upperRight1ToothDecay;
    private String upperRight1Repaired;
    private String upperRight1Extracted;

    private String upperRight2Healthy;
    private String upperRight2ToothDecay;
    private String upperRight2Repaired;
    private String upperRight2Extracted;

    private String upperRight3Healthy;
    private String upperRight3ToothDecay;
    private String upperRight3Repaired;
    private String upperRight3Extracted;

    private String upperRight4Healthy;
    private String upperRight4ToothDecay;
    private String upperRight4Repaired;
    private String upperRight4Extracted;

    private String upperRight5Healthy;
    private String upperRight5ToothDecay;
    private String upperRight5Repaired;
    private String upperRight5Extracted;

    private String upperRight6Healthy;
    private String upperRight6ToothDecay;
    private String upperRight6Repaired;
    private String upperRight6Extracted;

    private String upperRight7Healthy;
    private String upperRight7ToothDecay;
    private String upperRight7Repaired;
    private String upperRight7Extracted;

    private String upperRight8Healthy;
    private String upperRight8ToothDecay;
    private String upperRight8Repaired;
    private String upperRight8Extracted;

    private String lowerLeft1Healthy;
    private String lowerLeft1ToothDecay;
    private String lowerLeft1Repaired;
    private String lowerLeft1Extracted;

    private String lowerLeft2Healthy;
    private String lowerLeft2ToothDecay;
    private String lowerLeft2Repaired;
    private String lowerLeft2Extracted;

    private String lowerLeft3Healthy;
    private String lowerLeft3ToothDecay;
    private String lowerLeft3Repaired;
    private String lowerLeft3Extracted;

    private String lowerLeft4Healthy;
    private String lowerLeft4ToothDecay;
    private String lowerLeft4Repaired;
    private String lowerLeft4Extracted;

    private String lowerLeft5Healthy;
    private String lowerLeft5ToothDecay;
    private String lowerLeft5Repaired;
    private String lowerLeft5Extracted;

    private String lowerLeft6Healthy;
    private String lowerLeft6ToothDecay;
    private String lowerLeft6Repaired;
    private String lowerLeft6Extracted;

    private String lowerLeft7Healthy;
    private String lowerLeft7ToothDecay;
    private String lowerLeft7Repaired;
    private String lowerLeft7Extracted;

    private String lowerLeft8Healthy;
    private String lowerLeft8ToothDecay;
    private String lowerLeft8Repaired;
    private String lowerLeft8Extracted;

    private String lowerRight1Healthy;
    private String lowerRight1ToothDecay;
    private String lowerRight1Repaired;
    private String lowerRight1Extracted;

    private String lowerRight2Healthy;
    private String lowerRight2ToothDecay;
    private String lowerRight2Repaired;
    private String lowerRight2Extracted;

    private String lowerRight3Healthy;
    private String lowerRight3ToothDecay;
    private String lowerRight3Repaired;
    private String lowerRight3Extracted;

    private String lowerRight4Healthy;
    private String lowerRight4ToothDecay;
    private String lowerRight4Repaired;
    private String lowerRight4Extracted;

    private String lowerRight5Healthy;
    private String lowerRight5ToothDecay;
    private String lowerRight5Repaired;
    private String lowerRight5Extracted;

    private String lowerRight6Healthy;
    private String lowerRight6ToothDecay;
    private String lowerRight6Repaired;
    private String lowerRight6Extracted;

    private String lowerRight7Healthy;
    private String lowerRight7ToothDecay;
    private String lowerRight7Repaired;
    private String lowerRight7Extracted;

    private String lowerRight8Healthy;
    private String lowerRight8ToothDecay;
    private String lowerRight8Repaired;
    private String lowerRight8Extracted;
}