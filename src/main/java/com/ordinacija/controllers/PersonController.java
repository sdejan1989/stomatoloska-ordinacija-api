package com.ordinacija.controllers;

import com.ordinacija.dto.CreatePersonRequestDto;
import com.ordinacija.model.Person;
import com.ordinacija.services.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(PersonController.BASE_URL)
@AllArgsConstructor
public class PersonController {

    static final String BASE_URL = "/person";

    private final PersonService personService;

    @PostMapping()
    public Person savePerson(@Valid @RequestBody CreatePersonRequestDto person){
        if (person.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
         }

        Person personToSave = new Person();
        personToSave.setId(person.getId());
        personToSave.setFirstName(person.getBasicData().getFirstName());
        personToSave.setLastName(person.getBasicData().getLastName());
        personToSave.setBirthDate(person.getBasicData().getBirthDate());
        personToSave.setCity(person.getBasicData().getCity());
        personToSave.setCountry(person.getBasicData().getCountry());
        personToSave.setPhoneNumber(person.getBasicData().getPhoneNumber());
        personToSave.setUpperLeft1Healthy(person.getTeeth().getUpperLeft().getTooth().get(0).getHealthy());
        personToSave.setUpperLeft1ToothDecay(person.getTeeth().getUpperLeft().getTooth().get(0).getToothDecay());
        personToSave.setUpperLeft1Repaired(person.getTeeth().getUpperLeft().getTooth().get(0).getRepaired());
        personToSave.setUpperLeft1Extracted(person.getTeeth().getUpperLeft().getTooth().get(0).getExtracted());
        personToSave.setUpperLeft2Healthy(person.getTeeth().getUpperLeft().getTooth().get(1).getHealthy());
        personToSave.setUpperLeft2ToothDecay(person.getTeeth().getUpperLeft().getTooth().get(1).getToothDecay());
        personToSave.setUpperLeft2Repaired(person.getTeeth().getUpperLeft().getTooth().get(1).getRepaired());
        personToSave.setUpperLeft2Extracted(person.getTeeth().getUpperLeft().getTooth().get(1).getExtracted());
        personToSave.setUpperLeft3Healthy(person.getTeeth().getUpperLeft().getTooth().get(2).getHealthy());
        personToSave.setUpperLeft3ToothDecay(person.getTeeth().getUpperLeft().getTooth().get(2).getToothDecay());
        personToSave.setUpperLeft3Repaired(person.getTeeth().getUpperLeft().getTooth().get(2).getRepaired());
        personToSave.setUpperLeft3Extracted(person.getTeeth().getUpperLeft().getTooth().get(2).getExtracted());
        personToSave.setUpperLeft4Healthy(person.getTeeth().getUpperLeft().getTooth().get(3).getHealthy());
        personToSave.setUpperLeft4ToothDecay(person.getTeeth().getUpperLeft().getTooth().get(3).getToothDecay());
        personToSave.setUpperLeft4Repaired(person.getTeeth().getUpperLeft().getTooth().get(3).getRepaired());
        personToSave.setUpperLeft4Extracted(person.getTeeth().getUpperLeft().getTooth().get(3).getExtracted());
        personToSave.setUpperLeft5Healthy(person.getTeeth().getUpperLeft().getTooth().get(4).getHealthy());
        personToSave.setUpperLeft5ToothDecay(person.getTeeth().getUpperLeft().getTooth().get(4).getToothDecay());
        personToSave.setUpperLeft5Repaired(person.getTeeth().getUpperLeft().getTooth().get(4).getRepaired());
        personToSave.setUpperLeft5Extracted(person.getTeeth().getUpperLeft().getTooth().get(4).getExtracted());
        personToSave.setUpperLeft6Healthy(person.getTeeth().getUpperLeft().getTooth().get(5).getHealthy());
        personToSave.setUpperLeft6ToothDecay(person.getTeeth().getUpperLeft().getTooth().get(5).getToothDecay());
        personToSave.setUpperLeft6Repaired(person.getTeeth().getUpperLeft().getTooth().get(5).getRepaired());
        personToSave.setUpperLeft6Extracted(person.getTeeth().getUpperLeft().getTooth().get(5).getExtracted());
        personToSave.setUpperLeft7Healthy(person.getTeeth().getUpperLeft().getTooth().get(6).getHealthy());
        personToSave.setUpperLeft7ToothDecay(person.getTeeth().getUpperLeft().getTooth().get(6).getToothDecay());
        personToSave.setUpperLeft7Repaired(person.getTeeth().getUpperLeft().getTooth().get(6).getRepaired());
        personToSave.setUpperLeft7Extracted(person.getTeeth().getUpperLeft().getTooth().get(6).getExtracted());
        personToSave.setUpperLeft8Healthy(person.getTeeth().getUpperLeft().getTooth().get(7).getHealthy());
        personToSave.setUpperLeft8ToothDecay(person.getTeeth().getUpperLeft().getTooth().get(7).getToothDecay());
        personToSave.setUpperLeft8Repaired(person.getTeeth().getUpperLeft().getTooth().get(7).getRepaired());
        personToSave.setUpperLeft8Extracted(person.getTeeth().getUpperLeft().getTooth().get(7).getExtracted());
        personToSave.setUpperRight1Healthy(person.getTeeth().getUpperRight().getTooth().get(0).getHealthy());
        personToSave.setUpperRight1ToothDecay(person.getTeeth().getUpperRight().getTooth().get(0).getToothDecay());
        personToSave.setUpperRight1Repaired(person.getTeeth().getUpperRight().getTooth().get(0).getRepaired());
        personToSave.setUpperRight1Extracted(person.getTeeth().getUpperRight().getTooth().get(0).getExtracted());
        personToSave.setUpperRight2Healthy(person.getTeeth().getUpperRight().getTooth().get(1).getHealthy());
        personToSave.setUpperRight2ToothDecay(person.getTeeth().getUpperRight().getTooth().get(1).getToothDecay());
        personToSave.setUpperRight2Repaired(person.getTeeth().getUpperRight().getTooth().get(1).getRepaired());
        personToSave.setUpperRight2Extracted(person.getTeeth().getUpperRight().getTooth().get(1).getExtracted());
        personToSave.setUpperRight3Healthy(person.getTeeth().getUpperRight().getTooth().get(2).getHealthy());
        personToSave.setUpperRight3ToothDecay(person.getTeeth().getUpperRight().getTooth().get(2).getToothDecay());
        personToSave.setUpperRight3Repaired(person.getTeeth().getUpperRight().getTooth().get(2).getRepaired());
        personToSave.setUpperRight3Extracted(person.getTeeth().getUpperRight().getTooth().get(2).getExtracted());
        personToSave.setUpperRight4Healthy(person.getTeeth().getUpperRight().getTooth().get(3).getHealthy());
        personToSave.setUpperRight4ToothDecay(person.getTeeth().getUpperRight().getTooth().get(3).getToothDecay());
        personToSave.setUpperRight4Repaired(person.getTeeth().getUpperRight().getTooth().get(3).getRepaired());
        personToSave.setUpperRight4Extracted(person.getTeeth().getUpperRight().getTooth().get(3).getExtracted());
        personToSave.setUpperRight5Healthy(person.getTeeth().getUpperRight().getTooth().get(4).getHealthy());
        personToSave.setUpperRight5ToothDecay(person.getTeeth().getUpperRight().getTooth().get(4).getToothDecay());
        personToSave.setUpperRight5Repaired(person.getTeeth().getUpperRight().getTooth().get(4).getRepaired());
        personToSave.setUpperRight5Extracted(person.getTeeth().getUpperRight().getTooth().get(4).getExtracted());
        personToSave.setUpperRight6Healthy(person.getTeeth().getUpperRight().getTooth().get(5).getHealthy());
        personToSave.setUpperRight6ToothDecay(person.getTeeth().getUpperRight().getTooth().get(5).getToothDecay());
        personToSave.setUpperRight6Repaired(person.getTeeth().getUpperRight().getTooth().get(5).getRepaired());
        personToSave.setUpperRight6Extracted(person.getTeeth().getUpperRight().getTooth().get(5).getExtracted());
        personToSave.setUpperRight7Healthy(person.getTeeth().getUpperRight().getTooth().get(6).getHealthy());
        personToSave.setUpperRight7ToothDecay(person.getTeeth().getUpperRight().getTooth().get(6).getToothDecay());
        personToSave.setUpperRight7Repaired(person.getTeeth().getUpperRight().getTooth().get(6).getRepaired());
        personToSave.setUpperRight7Extracted(person.getTeeth().getUpperRight().getTooth().get(6).getExtracted());
        personToSave.setUpperRight8Healthy(person.getTeeth().getUpperRight().getTooth().get(7).getHealthy());
        personToSave.setUpperRight8ToothDecay(person.getTeeth().getUpperRight().getTooth().get(7).getToothDecay());
        personToSave.setUpperRight8Repaired(person.getTeeth().getUpperRight().getTooth().get(7).getRepaired());
        personToSave.setUpperRight8Extracted(person.getTeeth().getUpperRight().getTooth().get(7).getExtracted());
        personToSave.setLowerLeft1Healthy(person.getTeeth().getLowerLeft().getTooth().get(0).getHealthy());
        personToSave.setLowerLeft1ToothDecay(person.getTeeth().getLowerLeft().getTooth().get(0).getToothDecay());
        personToSave.setLowerLeft1Repaired(person.getTeeth().getLowerLeft().getTooth().get(0).getRepaired());
        personToSave.setLowerLeft1Extracted(person.getTeeth().getLowerLeft().getTooth().get(0).getExtracted());
        personToSave.setLowerLeft2Healthy(person.getTeeth().getLowerLeft().getTooth().get(1).getHealthy());
        personToSave.setLowerLeft2ToothDecay(person.getTeeth().getLowerLeft().getTooth().get(1).getToothDecay());
        personToSave.setLowerLeft2Repaired(person.getTeeth().getLowerLeft().getTooth().get(1).getRepaired());
        personToSave.setLowerLeft2Extracted(person.getTeeth().getLowerLeft().getTooth().get(1).getExtracted());
        personToSave.setLowerLeft3Healthy(person.getTeeth().getLowerLeft().getTooth().get(2).getHealthy());
        personToSave.setLowerLeft3ToothDecay(person.getTeeth().getLowerLeft().getTooth().get(2).getToothDecay());
        personToSave.setLowerLeft3Repaired(person.getTeeth().getLowerLeft().getTooth().get(2).getRepaired());
        personToSave.setLowerLeft3Extracted(person.getTeeth().getLowerLeft().getTooth().get(2).getExtracted());
        personToSave.setLowerLeft4Healthy(person.getTeeth().getLowerLeft().getTooth().get(3).getHealthy());
        personToSave.setLowerLeft4ToothDecay(person.getTeeth().getLowerLeft().getTooth().get(3).getToothDecay());
        personToSave.setLowerLeft4Repaired(person.getTeeth().getLowerLeft().getTooth().get(3).getRepaired());
        personToSave.setLowerLeft4Extracted(person.getTeeth().getLowerLeft().getTooth().get(3).getExtracted());
        personToSave.setLowerLeft5Healthy(person.getTeeth().getLowerLeft().getTooth().get(4).getHealthy());
        personToSave.setLowerLeft5ToothDecay(person.getTeeth().getLowerLeft().getTooth().get(4).getToothDecay());
        personToSave.setLowerLeft5Repaired(person.getTeeth().getLowerLeft().getTooth().get(4).getRepaired());
        personToSave.setLowerLeft5Extracted(person.getTeeth().getLowerLeft().getTooth().get(4).getExtracted());
        personToSave.setLowerLeft6Healthy(person.getTeeth().getLowerLeft().getTooth().get(5).getHealthy());
        personToSave.setLowerLeft6ToothDecay(person.getTeeth().getLowerLeft().getTooth().get(5).getToothDecay());
        personToSave.setLowerLeft6Repaired(person.getTeeth().getLowerLeft().getTooth().get(5).getRepaired());
        personToSave.setLowerLeft6Extracted(person.getTeeth().getLowerLeft().getTooth().get(5).getExtracted());
        personToSave.setLowerLeft7Healthy(person.getTeeth().getLowerLeft().getTooth().get(6).getHealthy());
        personToSave.setLowerLeft7ToothDecay(person.getTeeth().getLowerLeft().getTooth().get(6).getToothDecay());
        personToSave.setLowerLeft7Repaired(person.getTeeth().getLowerLeft().getTooth().get(6).getRepaired());
        personToSave.setLowerLeft7Extracted(person.getTeeth().getLowerLeft().getTooth().get(6).getExtracted());
        personToSave.setLowerLeft8Healthy(person.getTeeth().getLowerLeft().getTooth().get(7).getHealthy());
        personToSave.setLowerLeft8ToothDecay(person.getTeeth().getLowerLeft().getTooth().get(7).getToothDecay());
        personToSave.setLowerLeft8Repaired(person.getTeeth().getLowerLeft().getTooth().get(7).getRepaired());
        personToSave.setLowerLeft8Extracted(person.getTeeth().getLowerLeft().getTooth().get(7).getExtracted());
        personToSave.setLowerRight1Healthy(person.getTeeth().getLowerRight().getTooth().get(0).getHealthy());
        personToSave.setLowerRight1ToothDecay(person.getTeeth().getLowerRight().getTooth().get(0).getToothDecay());
        personToSave.setLowerRight1Repaired(person.getTeeth().getLowerRight().getTooth().get(0).getRepaired());
        personToSave.setLowerRight1Extracted(person.getTeeth().getLowerRight().getTooth().get(0).getExtracted());
        personToSave.setLowerRight2Healthy(person.getTeeth().getLowerRight().getTooth().get(1).getHealthy());
        personToSave.setLowerRight2ToothDecay(person.getTeeth().getLowerRight().getTooth().get(1).getToothDecay());
        personToSave.setLowerRight2Repaired(person.getTeeth().getLowerRight().getTooth().get(1).getRepaired());
        personToSave.setLowerRight2Extracted(person.getTeeth().getLowerRight().getTooth().get(1).getExtracted());
        personToSave.setLowerRight3Healthy(person.getTeeth().getLowerRight().getTooth().get(2).getHealthy());
        personToSave.setLowerRight3ToothDecay(person.getTeeth().getLowerRight().getTooth().get(2).getToothDecay());
        personToSave.setLowerRight3Repaired(person.getTeeth().getLowerRight().getTooth().get(2).getRepaired());
        personToSave.setLowerRight3Extracted(person.getTeeth().getLowerRight().getTooth().get(2).getExtracted());
        personToSave.setLowerRight4Healthy(person.getTeeth().getLowerRight().getTooth().get(3).getHealthy());
        personToSave.setLowerRight4ToothDecay(person.getTeeth().getLowerRight().getTooth().get(3).getToothDecay());
        personToSave.setLowerRight4Repaired(person.getTeeth().getLowerRight().getTooth().get(3).getRepaired());
        personToSave.setLowerRight4Extracted(person.getTeeth().getLowerRight().getTooth().get(3).getExtracted());
        personToSave.setLowerRight5Healthy(person.getTeeth().getLowerRight().getTooth().get(4).getHealthy());
        personToSave.setLowerRight5ToothDecay(person.getTeeth().getLowerRight().getTooth().get(4).getToothDecay());
        personToSave.setLowerRight5Repaired(person.getTeeth().getLowerRight().getTooth().get(4).getRepaired());
        personToSave.setLowerRight5Extracted(person.getTeeth().getLowerRight().getTooth().get(4).getExtracted());
        personToSave.setLowerRight6Healthy(person.getTeeth().getLowerRight().getTooth().get(5).getHealthy());
        personToSave.setLowerRight6ToothDecay(person.getTeeth().getLowerRight().getTooth().get(5).getToothDecay());
        personToSave.setLowerRight6Repaired(person.getTeeth().getLowerRight().getTooth().get(5).getRepaired());
        personToSave.setLowerRight6Extracted(person.getTeeth().getLowerRight().getTooth().get(5).getExtracted());
        personToSave.setLowerRight7Healthy(person.getTeeth().getLowerRight().getTooth().get(6).getHealthy());
        personToSave.setLowerRight7ToothDecay(person.getTeeth().getLowerRight().getTooth().get(6).getToothDecay());
        personToSave.setLowerRight7Repaired(person.getTeeth().getLowerRight().getTooth().get(6).getRepaired());
        personToSave.setLowerRight7Extracted(person.getTeeth().getLowerRight().getTooth().get(6).getExtracted());
        personToSave.setLowerRight8Healthy(person.getTeeth().getLowerRight().getTooth().get(7).getHealthy());
        personToSave.setLowerRight8ToothDecay(person.getTeeth().getLowerRight().getTooth().get(7).getToothDecay());
        personToSave.setLowerRight8Repaired(person.getTeeth().getLowerRight().getTooth().get(7).getRepaired());
        personToSave.setLowerRight8Extracted(person.getTeeth().getLowerRight().getTooth().get(7).getExtracted());

        return personService.savePerson(personToSave);
    }

    @GetMapping()
    List<Person> getAllPersons(){
        return personService.findAllPersons();
    }

    @GetMapping("/{id}")
    public Person getPersonById(@PathVariable Long id){
        return personService.findPersonById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        personService.delete(id);
    }

    @PutMapping("/{id}")
    public Person update(@Valid @RequestBody Person updatedPerson, @PathVariable Long id) {
        if (updatedPerson.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID key not allowed");
        }
        return personService.update(updatedPerson, id);
    }
}
