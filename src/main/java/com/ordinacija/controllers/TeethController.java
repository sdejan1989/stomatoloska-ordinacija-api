//package com.ordinacija.controllers;
//
//import com.ordinacija.dto.Teeth;
//import com.ordinacija.services.TeethService;
//import lombok.AllArgsConstructor;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import javax.validation.Valid;
//import java.util.List;
//
//@CrossOrigin(origins = "http://localhost:4200")
//@RestController
//@RequestMapping(TeethController.BASE_URL)
//@AllArgsConstructor
//public class TeethController {
//    static final String BASE_URL = "/person";
//
//    private final TeethService teethService;
//
//    @PostMapping()
//    public Teeth saveTeeth(@Valid @RequestBody Teeth teeth){
//        if (teeth.getId() != null) {
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
//        }
//        return teethService.saveTeeth(teeth);
//    }
//
//    @GetMapping()
//    List<Teeth> getAllTeeth(){
//        return teethService.findAllTeeth();
//    }
//
//    @GetMapping("/{id}")
//    public Teeth getTeethById(@PathVariable Long id){
//        return teethService.findTeethById(id);
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@PathVariable Long id) {
//        teethService.delete(id);
//    }
//
//    @PutMapping("/{id}")
//    public Teeth update(@Valid @RequestBody Teeth updatedTeeth, @PathVariable Long id) {
//        if (updatedTeeth.getId() != null) {
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID key not allowed");
//        }
//        return teethService.update(updatedTeeth, id);
//    }
//}
