//package com.ordinacija.services;
//
//import com.ordinacija.dto.Teeth;
//import com.ordinacija.repositories.TeethRepository;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//import org.springframework.web.server.ResponseStatusException;
//
//import java.util.List;
//
//@Service
//public class TeethServiceImpl implements TeethService {
//
//    private final TeethRepository teethRepository;
//
//    public TeethServiceImpl(TeethRepository teethRepository) {
//        this.teethRepository = teethRepository;
//    }
//
//    @Override
//    public Teeth findTeethById(Long id) {
//        if (!teethRepository.existsById(id)) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No teeth with id " + id + " exist");
//        }
//        return teethRepository.findById(id).get();
//    }
//
//    @Override
//    public List<Teeth> findAllTeeth() {
//        return teethRepository.findAll();
//    }
//
//    @Override
//    public Teeth saveTeeth(Teeth teeth) {
//        return teethRepository.save(teeth);
//    }
//
//    @Override
//    public void delete(Long id) {
//
//        if (!teethRepository.existsById(id)) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No teeth with id " + id + " exist");
//        }
//        teethRepository.deleteById(id);
//    }
//
//    @Override
//    public Teeth update(Teeth updatedTeeth, Long id) {
//
//        Teeth teeth = findTeethById(id);
//
//        teeth.setUpperLeft1(updatedTeeth.getUpperLeft1());
//        teeth.setUpperLeft2(updatedTeeth.getUpperLeft2());
//        teeth.setUpperLeft3(updatedTeeth.getUpperLeft3());
//        teeth.setUpperLeft4(updatedTeeth.getUpperLeft4());
//        teeth.setUpperLeft5(updatedTeeth.getUpperLeft5());
//        teeth.setUpperLeft6(updatedTeeth.getUpperLeft6());
//        teeth.setUpperLeft7(updatedTeeth.getUpperLeft7());
//        teeth.setUpperLeft8(updatedTeeth.getUpperLeft8());
//        teeth.setUpperRight1(updatedTeeth.getUpperRight1());
//        teeth.setUpperRight2(updatedTeeth.getUpperRight2());
//        teeth.setUpperRight3(updatedTeeth.getUpperRight3());
//        teeth.setUpperRight4(updatedTeeth.getUpperRight4());
//        teeth.setUpperRight5(updatedTeeth.getUpperRight5());
//        teeth.setUpperRight6(updatedTeeth.getUpperRight6());
//        teeth.setUpperRight7(updatedTeeth.getUpperRight7());
//        teeth.setUpperRight8(updatedTeeth.getUpperRight8());
//        teeth.setLowerLeft1(updatedTeeth.getLowerLeft1());
//        teeth.setLowerLeft2(updatedTeeth.getLowerLeft2());
//        teeth.setLowerLeft3(updatedTeeth.getLowerLeft3());
//        teeth.setLowerLeft4(updatedTeeth.getLowerLeft4());
//        teeth.setLowerLeft5(updatedTeeth.getLowerLeft5());
//        teeth.setLowerLeft6(updatedTeeth.getLowerLeft6());
//        teeth.setLowerLeft7(updatedTeeth.getLowerLeft7());
//        teeth.setLowerLeft8(updatedTeeth.getLowerLeft8());
//        teeth.setLowerRight1(updatedTeeth.getLowerRight1());
//        teeth.setLowerRight2(updatedTeeth.getLowerRight2());
//        teeth.setLowerRight3(updatedTeeth.getLowerRight3());
//        teeth.setLowerRight4(updatedTeeth.getLowerRight4());
//        teeth.setLowerRight5(updatedTeeth.getLowerRight5());
//        teeth.setLowerRight6(updatedTeeth.getLowerRight6());
//        teeth.setLowerRight7(updatedTeeth.getLowerRight7());
//        teeth.setLowerRight8(updatedTeeth.getLowerRight8());
//
//        return teethRepository.save(teeth);
//    }
//
//}
