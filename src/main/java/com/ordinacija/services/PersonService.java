package com.ordinacija.services;

import com.ordinacija.model.Person;

import java.util.List;

public interface PersonService {

    Person findPersonById(Long id);

    List<Person> findAllPersons();

    Person savePerson(Person person);

    void delete(Long id);

    Person update(Person updatePerson, Long id);
}
