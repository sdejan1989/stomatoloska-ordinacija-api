package com.ordinacija.services;

import com.ordinacija.model.Person;
import com.ordinacija.repositories.PersonRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class PersonImpl implements PersonService {

    private final PersonRepository personRepository;

    public PersonImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person findPersonById(Long id) {
        if (!personRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No basic data with id " + id + " exist");
        }
        return personRepository.findById(id).get();
    }

    @Override
    public List<Person> findAllPersons() {
        return personRepository.findAll();
    }

    @Override
    public Person savePerson(Person person) {

        return personRepository.save(person);
    }

    @Override
    public void delete(Long id) {

        if (!personRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data with id " + id + " exist");
        }
        personRepository.deleteById(id);
    }

    @Override
    public Person update(Person updatedPerson, Long id) {

        Person person = findPersonById(id);

        person.setFirstName(updatedPerson.getFirstName());
        person.setLastName(updatedPerson.getFirstName());
        person.setBirthDate(updatedPerson.getBirthDate());
        person.setCity(updatedPerson.getCity());
        person.setCountry(updatedPerson.getCountry());
        person.setPhoneNumber(updatedPerson.getPhoneNumber());

        person.setUpperLeft1Healthy(updatedPerson.getUpperLeft1Healthy());
        person.setUpperLeft1ToothDecay(updatedPerson.getUpperLeft1ToothDecay());
        person.setUpperLeft1Repaired(updatedPerson.getUpperLeft1Repaired());
        person.setUpperLeft1Extracted(updatedPerson.getUpperLeft1Extracted());

        person.setUpperLeft2Healthy(updatedPerson.getUpperLeft2Healthy());
        person.setUpperLeft2ToothDecay(updatedPerson.getUpperLeft2ToothDecay());
        person.setUpperLeft2Repaired(updatedPerson.getUpperLeft2Repaired());
        person.setUpperLeft2Extracted(updatedPerson.getUpperLeft2Extracted());

        person.setUpperLeft3Healthy(updatedPerson.getUpperLeft3Healthy());
        person.setUpperLeft3ToothDecay(updatedPerson.getUpperLeft3ToothDecay());
        person.setUpperLeft3Repaired(updatedPerson.getUpperLeft3Repaired());
        person.setUpperLeft3Extracted(updatedPerson.getUpperLeft3Extracted());

        person.setUpperLeft4Healthy(updatedPerson.getUpperLeft4Healthy());
        person.setUpperLeft4ToothDecay(updatedPerson.getUpperLeft4ToothDecay());
        person.setUpperLeft4Repaired(updatedPerson.getUpperLeft4Repaired());
        person.setUpperLeft4Extracted(updatedPerson.getUpperLeft4Extracted());

        person.setUpperLeft5Healthy(updatedPerson.getUpperLeft5Healthy());
        person.setUpperLeft5ToothDecay(updatedPerson.getUpperLeft5ToothDecay());
        person.setUpperLeft5Repaired(updatedPerson.getUpperLeft5Repaired());
        person.setUpperLeft5Extracted(updatedPerson.getUpperLeft5Extracted());

        person.setUpperLeft6Healthy(updatedPerson.getUpperLeft6Healthy());
        person.setUpperLeft6ToothDecay(updatedPerson.getUpperLeft6ToothDecay());
        person.setUpperLeft6Repaired(updatedPerson.getUpperLeft6Repaired());
        person.setUpperLeft6Extracted(updatedPerson.getUpperLeft6Extracted());

        person.setUpperLeft7Healthy(updatedPerson.getUpperLeft7Healthy());
        person.setUpperLeft7ToothDecay(updatedPerson.getUpperLeft7ToothDecay());
        person.setUpperLeft7Repaired(updatedPerson.getUpperLeft7Repaired());
        person.setUpperLeft7Extracted(updatedPerson.getUpperLeft7Extracted());

        person.setUpperLeft8Healthy(updatedPerson.getUpperLeft8Healthy());
        person.setUpperLeft8ToothDecay(updatedPerson.getUpperLeft8ToothDecay());
        person.setUpperLeft8Repaired(updatedPerson.getUpperLeft8Repaired());
        person.setUpperLeft8Extracted(updatedPerson.getUpperLeft8Extracted());

        person.setUpperRight1Healthy(updatedPerson.getUpperRight1Healthy());
        person.setUpperRight1ToothDecay(updatedPerson.getUpperRight1ToothDecay());
        person.setUpperRight1Repaired(updatedPerson.getUpperRight1Repaired());
        person.setUpperRight1Extracted(updatedPerson.getUpperRight1Extracted());

        person.setUpperRight2Healthy(updatedPerson.getUpperRight2Healthy());
        person.setUpperRight2ToothDecay(updatedPerson.getUpperRight2ToothDecay());
        person.setUpperRight2Repaired(updatedPerson.getUpperRight2Repaired());
        person.setUpperRight2Extracted(updatedPerson.getUpperRight2Extracted());

        person.setUpperRight3Healthy(updatedPerson.getUpperRight3Healthy());
        person.setUpperRight3ToothDecay(updatedPerson.getUpperRight3ToothDecay());
        person.setUpperRight3Repaired(updatedPerson.getUpperRight3Repaired());
        person.setUpperRight3Extracted(updatedPerson.getUpperRight3Extracted());

        person.setUpperRight4Healthy(updatedPerson.getUpperRight4Healthy());
        person.setUpperRight4ToothDecay(updatedPerson.getUpperRight4ToothDecay());
        person.setUpperRight4Repaired(updatedPerson.getUpperRight4Repaired());
        person.setUpperRight4Extracted(updatedPerson.getUpperRight4Extracted());

        person.setUpperRight5Healthy(updatedPerson.getUpperRight5Healthy());
        person.setUpperRight5ToothDecay(updatedPerson.getUpperRight5ToothDecay());
        person.setUpperRight5Repaired(updatedPerson.getUpperRight5Repaired());
        person.setUpperRight5Extracted(updatedPerson.getUpperRight5Extracted());

        person.setUpperRight6Healthy(updatedPerson.getUpperRight6Healthy());
        person.setUpperRight6ToothDecay(updatedPerson.getUpperRight6ToothDecay());
        person.setUpperRight6Repaired(updatedPerson.getUpperRight6Repaired());
        person.setUpperRight6Extracted(updatedPerson.getUpperRight6Extracted());

        person.setUpperRight7Healthy(updatedPerson.getUpperRight7Healthy());
        person.setUpperRight7ToothDecay(updatedPerson.getUpperRight7ToothDecay());
        person.setUpperRight7Repaired(updatedPerson.getUpperRight7Repaired());
        person.setUpperRight7Extracted(updatedPerson.getUpperRight7Extracted());

        person.setUpperRight8Healthy(updatedPerson.getUpperRight8Healthy());
        person.setUpperRight8ToothDecay(updatedPerson.getUpperRight8ToothDecay());
        person.setUpperRight8Repaired(updatedPerson.getUpperRight8Repaired());
        person.setUpperRight8Extracted(updatedPerson.getUpperRight8Extracted());

        person.setLowerLeft1Healthy(updatedPerson.getLowerLeft1Healthy());
        person.setLowerLeft1ToothDecay(updatedPerson.getLowerLeft1ToothDecay());
        person.setLowerLeft1Repaired(updatedPerson.getLowerLeft1Repaired());
        person.setLowerLeft1Extracted(updatedPerson.getLowerLeft1Extracted());

        person.setLowerLeft2Healthy(updatedPerson.getLowerLeft2Healthy());
        person.setLowerLeft2ToothDecay(updatedPerson.getLowerLeft2ToothDecay());
        person.setLowerLeft2Repaired(updatedPerson.getLowerLeft2Repaired());
        person.setLowerLeft2Extracted(updatedPerson.getLowerLeft2Extracted());

        person.setLowerLeft3Healthy(updatedPerson.getLowerLeft3Healthy());
        person.setLowerLeft3ToothDecay(updatedPerson.getLowerLeft3ToothDecay());
        person.setLowerLeft3Repaired(updatedPerson.getLowerLeft3Repaired());
        person.setLowerLeft3Extracted(updatedPerson.getLowerLeft3Extracted());

        person.setLowerLeft4Healthy(updatedPerson.getLowerLeft4Healthy());
        person.setLowerLeft4ToothDecay(updatedPerson.getLowerLeft4ToothDecay());
        person.setLowerLeft4Repaired(updatedPerson.getLowerLeft4Repaired());
        person.setLowerLeft4Extracted(updatedPerson.getLowerLeft4Extracted());

        person.setLowerLeft5Healthy(updatedPerson.getLowerLeft5Healthy());
        person.setLowerLeft5ToothDecay(updatedPerson.getLowerLeft5ToothDecay());
        person.setLowerLeft5Repaired(updatedPerson.getLowerLeft5Repaired());
        person.setLowerLeft5Extracted(updatedPerson.getLowerLeft5Extracted());

        person.setLowerLeft6Healthy(updatedPerson.getLowerLeft6Healthy());
        person.setLowerLeft6ToothDecay(updatedPerson.getLowerLeft6ToothDecay());
        person.setLowerLeft6Repaired(updatedPerson.getLowerLeft6Repaired());
        person.setLowerLeft6Extracted(updatedPerson.getLowerLeft6Extracted());

        person.setLowerLeft7Healthy(updatedPerson.getLowerLeft7Healthy());
        person.setLowerLeft7ToothDecay(updatedPerson.getLowerLeft7ToothDecay());
        person.setLowerLeft7Repaired(updatedPerson.getLowerLeft7Repaired());
        person.setLowerLeft7Extracted(updatedPerson.getLowerLeft7Extracted());

        person.setLowerLeft8Healthy(updatedPerson.getLowerLeft8Healthy());
        person.setLowerLeft8ToothDecay(updatedPerson.getLowerLeft8ToothDecay());
        person.setLowerLeft8Repaired(updatedPerson.getLowerLeft8Repaired());
        person.setLowerLeft8Extracted(updatedPerson.getLowerLeft8Extracted());

        person.setLowerRight1Healthy(updatedPerson.getLowerRight1Healthy());
        person.setLowerRight1ToothDecay(updatedPerson.getLowerRight1ToothDecay());
        person.setLowerRight1Repaired(updatedPerson.getLowerRight1Repaired());
        person.setLowerRight1Extracted(updatedPerson.getLowerRight1Extracted());

        person.setLowerRight2Healthy(updatedPerson.getLowerRight2Healthy());
        person.setLowerRight2ToothDecay(updatedPerson.getLowerRight2ToothDecay());
        person.setLowerRight2Repaired(updatedPerson.getLowerRight2Repaired());
        person.setLowerRight2Extracted(updatedPerson.getLowerRight2Extracted());

        person.setLowerRight3Healthy(updatedPerson.getLowerRight3Healthy());
        person.setLowerRight3ToothDecay(updatedPerson.getLowerRight3ToothDecay());
        person.setLowerRight3Repaired(updatedPerson.getLowerRight3Repaired());
        person.setLowerRight3Extracted(updatedPerson.getLowerRight3Extracted());

        person.setLowerRight4Healthy(updatedPerson.getLowerRight4Healthy());
        person.setLowerRight4ToothDecay(updatedPerson.getLowerRight4ToothDecay());
        person.setLowerRight4Repaired(updatedPerson.getLowerRight4Repaired());
        person.setLowerRight4Extracted(updatedPerson.getLowerRight4Extracted());

        person.setLowerRight5Healthy(updatedPerson.getLowerRight5Healthy());
        person.setLowerRight5ToothDecay(updatedPerson.getLowerRight5ToothDecay());
        person.setLowerRight5Repaired(updatedPerson.getLowerRight5Repaired());
        person.setLowerRight5Extracted(updatedPerson.getLowerRight5Extracted());

        person.setLowerRight6Healthy(updatedPerson.getLowerRight6Healthy());
        person.setLowerRight6ToothDecay(updatedPerson.getLowerRight6ToothDecay());
        person.setLowerRight6Repaired(updatedPerson.getLowerRight6Repaired());
        person.setLowerRight6Extracted(updatedPerson.getLowerRight6Extracted());

        person.setLowerRight7Healthy(updatedPerson.getLowerRight7Healthy());
        person.setLowerRight7ToothDecay(updatedPerson.getLowerRight7ToothDecay());
        person.setLowerRight7Repaired(updatedPerson.getLowerRight7Repaired());
        person.setLowerRight7Extracted(updatedPerson.getLowerRight7Extracted());

        person.setLowerRight8Healthy(updatedPerson.getLowerRight8Healthy());
        person.setLowerRight8ToothDecay(updatedPerson.getLowerRight8ToothDecay());
        person.setLowerRight8Repaired(updatedPerson.getLowerRight8Repaired());
        person.setLowerRight8Extracted(updatedPerson.getLowerRight8Extracted());

        return personRepository.save(person);
    }

}
