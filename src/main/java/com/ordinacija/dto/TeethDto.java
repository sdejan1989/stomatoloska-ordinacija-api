package com.ordinacija.dto;

import lombok.Data;

@Data
public class TeethDto {
    private UpperLeftDto upperLeft;
    private UpperRightDto upperRight;
    private LowerLeftDto lowerLeft;
    private LowerRightDto lowerRight;
}
