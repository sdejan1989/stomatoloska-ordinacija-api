package com.ordinacija.dto;

import lombok.Data;

@Data
public class CreatePersonRequestDto {

    private Long id;

    private BasicDataDto basicData;

    private TeethDto teeth;
}
