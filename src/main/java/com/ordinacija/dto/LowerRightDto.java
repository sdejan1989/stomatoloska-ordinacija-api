package com.ordinacija.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LowerRightDto {
    private List<ToothDto> tooth = new ArrayList<>();
}
