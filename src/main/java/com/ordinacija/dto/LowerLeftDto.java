package com.ordinacija.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LowerLeftDto {
    private List<ToothDto> tooth = new ArrayList<>();
}
