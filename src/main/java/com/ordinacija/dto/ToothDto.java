package com.ordinacija.dto;

import lombok.Data;

@Data
public class ToothDto {
    private String healthy;
    private String toothDecay;
    private String repaired;
    private String extracted;
}
