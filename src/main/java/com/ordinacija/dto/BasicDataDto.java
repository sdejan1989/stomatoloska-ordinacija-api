package com.ordinacija.dto;

import lombok.Data;

@Data
public class BasicDataDto {

    private String firstName;
    private String lastName;
    private String birthDate;
    private String city;
    private String country;
    private String phoneNumber;
}
